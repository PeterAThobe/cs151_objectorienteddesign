
public class P1Setup implements GameState
{
	BattleShipController controller;
	
	P1Setup(BattleShipController c)
	{
		this.controller = c;
		placeOwnShip();
	}
	
	@Override
	public void placeOwnShip() 
	{
		this.controller.addShipCreator();
		this.controller.setNextListener();
	}
	@Override
	public void AttackEnemyShip() 
	{
		//game still setting up attack board inactive	
	}
	@Override
	public void swapPlayer() 
	{
		
		if(this.controller.getCurrentPlayerReady()) //true = player 1 has set all ships
		{
			this.controller.switchPlayer(); //switch currentPLayer
			this.controller.swapView(); //switch the Active View
			this.controller.setGameState(this.controller.getP2Setup()) ;//change to p2setup
			this.controller.updateStatus(); //update status info, repaint the frame
		}
		
	}
	@Override
	public void endGame() 
	{
		//nothing to do yet as game is being setup
	}
	
	@Override
	public String toString()
	{
		return "P1Setup" ;
	}
}
