import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
//add states to this class
public class BattleShipController 
{
	//Internal game states
	GameState p1Setup ; 
	GameState p2Setup ;
	GameState p1Turn ;
	GameState p2Turn ;
	GameState endGame ;
	
	GameState currentState ;
	
	// Player models/views
	private PlayerData enemy ;
	private PlayerScreen enemyView ;
	private PlayerData currentPlayer ;
	private PlayerScreen currentView ;
	
	private boolean AttackPositionSelected;//control to limit the player to attacking just one space 
	
	BattleShipController(PlayerData p1, PlayerData p2, PlayerScreen pv1, PlayerScreen pv2)
	{
		
		
		// player Models and Views
		this.currentPlayer = p1;
		this.currentView = pv1 ;

		this.enemy= p2 ;
		this.enemyView = pv2 ;
		
		
		// Game States
		this.p1Setup = new P1Setup(this) ;		
		this.currentState = this.p1Setup ;
		this.AttackPositionSelected = false;
		this.updateStatus();
		
	}
	
	
	public boolean getCurrentPlayerReady()
	{
		this.currentPlayer.checkReady();
		return this.currentPlayer.getReady();
	}
	
	public boolean getAttackable() // is this used??
	{
		return this.currentView.getAttackable();
	}
	
	public void switchPlayer()
	{
		PlayerData temp = this.currentPlayer ;
		this.currentPlayer = this.enemy ;
		this.enemy = temp ;
	}
	public boolean getAttackPositionSelected()
	{
		return this.AttackPositionSelected;
	}
	public void setAttackPositionSelectedFalse()
	{
		this.AttackPositionSelected = false;
	}
	public void setAttackPositionSelectedTrue()
	{
		this.AttackPositionSelected = true;
	}
	private void createShip(int row , int col)
	{
		currentPlayer.setShipPosition(row, col) ;
	}
	
	//adds the ships creator listener to selfgrid in the view
	public void addShipCreator()
	{
		for(int i = 0 ; i < 100; i++)
		{
			final int j = i;
			this.currentView.addSelfGridMouseListener(i, new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent e) 
				{
					int [] pos= currentView.getPanelPosition(j);
					
						createShip(pos[0], pos[1]) ;
					
					currentView.updateSelfGrid(currentPlayer.getShips());	
				}

				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
			});
		}
	}
	
	public void swapView()
	{
		PlayerScreen tempView = currentView ;
		this.currentView = this.enemyView ;
		this.enemyView = tempView ;
		this.currentView.updateAttackGrid(this.currentPlayer.getAttacked());
		this.currentView.updateSelfGrid(this.currentPlayer.getShips());
		this.currentView.repaint();
	}
	
	
	//adds the attack actionListener to the attack grid in the view
	public void addAttackListener() // fix 
	{
		for(int i = 0 ; i < 100 ; i++)
		{
			final int j = i;
			this.currentView.addAttackGridListener(i, new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent e) 
				{
					int[] position = currentView.getPanelPosition(j) ;
					if(!getAttackPositionSelected())
					{
						setAttackPositionSelectedTrue();
						boolean hit = attackPosition(position[0],position[1]);
						currentPlayer.setAttacked(position[0],position[1],hit);
						currentView.updateAttackGrid( currentPlayer.getAttacked() );
						currentView.updateSelfGrid(currentPlayer.getShips());
						updateStatus();
					}
				}

				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
			});
		}
	}
	
	private boolean attackPosition(int row, int col)
	{	
			this.AttackPositionSelected = true ;
			return this.enemy.attackShip(row, col) ;	
	}

	public void updateStatus()//need to finish
	{
		this.currentView.updatePlayerTag(currentPlayer.getTag()) ;
		this.currentView.updateShipsRemaining(currentPlayer.getActiveShips()) ;
		
		int n = this.enemy.getMaxShips() - this.enemy.getActiveShips(); // calculates enemy ships sunk
		this.currentView.updateEnemyShips(n) ;
		
		this.currentView.updateCurrentState(this.currentState.toString()); //displays the currentGameState
		
		this.currentView.repaint();
	}
	
	public void setNextListener()
	{
		this.currentView.setButtonListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				swapPlayer(); // use State method swapPlayer
			}
			
		});
		
	}
	
	public boolean checkForWinner()
	{
		if(this.enemy.getActiveShips() == 0)
		{
			return true;
		}
		return false;
	}
	
	
	// GameState functions 
	
	public void setGameState(GameState newGameState)
	{
		this.currentState = newGameState;
	}
	public GameState getP1Setup() 
	{
		if(this.p1Setup == null)
		{
			this.p1Setup = new P1Setup(this);
		}
		return this.p1Setup;
	}
	public GameState getP2Setup() 
	{
		if(this.p2Setup == null)
		{
			this.p2Setup = new P2Setup(this);
		}
		return this.p2Setup ; 
	}
	public GameState getP1Turn()  
	{
		if(this.p1Turn == null)
		{
			this.p1Turn = new P1Turn(this);
		}
		return this.p1Turn  ; 
	}
	public GameState getP2Turn()  
	{
		if(this.p2Turn == null)
		{
			this.p2Turn = new P2Turn(this);
		}
		return this.p2Turn  ; 
	}
	public GameState getEndGame() 
	{
		if(this.endGame == null)
		{
			this.endGame = new EndGame(this);
		}
		return this.endGame ; 
	}
	
	public void placeOwnShip()
	{
		this.currentState.placeOwnShip() ;
	}
	public void AttackEnemyShip()
	{
		this.currentState.AttackEnemyShip() ;
	}
	public void swapPlayer()
	{
		this.currentState.swapPlayer() ;
	}
	public String toString()
	{
		return this.currentState.toString();
	}
	
	public void displayWinner()
	{
		this.currentView.displayWinner(currentPlayer.getTag());
		this.currentView.repaint();
	}
	
	
}
