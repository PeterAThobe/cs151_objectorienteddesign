import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;

public abstract class BattleGrid extends JPanel 
{
	JPanel self;
    public BattleGrid() 
    {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        self = new JPanel();
        self.setLayout(new GridLayout(0,10));
        for (int i = 0 ; i < 10 ; i++)
        {
            for(int j =0 ; j < 10 ; j++) 
            {
                self.add(getCell());
            }
        }
        this.add(self);
    }
    
    
    protected abstract JPanel getCell();
    
    protected void addMListener(int i, MouseListener m )
    {
    		self.getComponent(i).addMouseListener(m) ;
    }
    
    protected void drawAttackBoard(Boolean [][] table)
    {
    		for(int i = 0 ; i < 10 ; i++)
    		{
    			for(int j = 0; j< 10 ; j++)
    			{
    				int position = (i*10) + j;
    				if(table[i][j] != null)
    				{
	    				if(table[i][j] == false)// miss
	    				{
	    					self.getComponent(position).setBackground(Color.red);
	    				}
	    				else if(table[i][j] == true)//hit
	    				{
	    					self.getComponent(position).setBackground(Color.green);
	    				}
    				}
    				else//not selected yet
    				{
    					self.getComponent(position).setBackground(Color.white);
    				}
    			}
    		}
    }
    protected void drawBoats(Ship[] ships)
    {
    		for(int i = 0 ; i < self.getComponentCount();i++)
    		{
    			self.getComponent(i).setBackground(Color.black);
    		}
    		for(int i = 0 ; i < ships.length ; i++)
    		{
    			if(!ships[i].getShipOrientation()) //horizontal orientation
    			{
    				if(ships[i].getShipSet())
    				{
	    				int s =  ships[i].getShipSize() ;
	    				int position = ships[i].getStart()[0] * 10 ;
	    				position += ships[i].getStart()[1] ;
	    				
	    				for(int j = 0 ; j < s ; j++)
	    				{
	    					if(!ships[i].checkSunk())
	    					{
	    						self.getComponent(position+j).setBackground(Color.green) ;
	    					}
	    					else 
	    					{
	    						self.getComponent(position+j).setBackground(Color.red) ;
	    					}
	    				}
    				}
    			}
    		}
    		
    
    }
    
    protected int[] getPosition(int i)
    {
    		int[] position= new int[2];
    		position[0] = self.getComponent(i).getY()/30  ; 
    		position[1] = self.getComponent(i).getX()/30 ;
    		
    		return position ;
    		
    }
    
    
}