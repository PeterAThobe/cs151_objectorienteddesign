import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;

/**
Represents the player's own grid
*/
public class SelfGrid extends BattleGrid
{
	private final int DIMENSION = 30;
	MouseListener mouse ;
    public SelfGrid(String name) 
    {
        super(); 
    }

    @Override
    protected JPanel getCell()
    {
        JPanel panel = new JPanel();
        panel.setBackground(Color.black);
        panel.setBorder(BorderFactory.createLineBorder(Color.gray, 5));
        panel.setPreferredSize(new Dimension(DIMENSION, DIMENSION)); // for demo purposes only
        
        return panel;
    }
    
}