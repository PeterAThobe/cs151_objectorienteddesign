
public class P1Turn implements GameState
{
	BattleShipController controller;
	
	P1Turn(BattleShipController c)
	{
		this.controller = c;
		this.AttackEnemyShip();
	}

	@Override
	public void placeOwnShip() 
	{
		//Game Already Started
	}

	@Override
	public void AttackEnemyShip() 
	{
		this.controller.addAttackListener();
	}

	@Override
	public void swapPlayer() 
	{ 
		if(!this.controller.checkForWinner())
		{
			if(this.controller.getAttackPositionSelected())
			{
				this.controller.setAttackPositionSelectedFalse();
				this.controller.switchPlayer();
				this.controller.swapView();
				this.controller.setGameState(this.controller.getP2Turn());
				this.controller.updateStatus();
			}
		}
		else
		{
			this.endGame();
		}
	}

	@Override
	public void endGame() 
	{
		this.controller.setGameState(this.controller.getEndGame());
	}
	
	@Override
	public String toString()
	{
		return "P1Turn" ;
	}
	

}
