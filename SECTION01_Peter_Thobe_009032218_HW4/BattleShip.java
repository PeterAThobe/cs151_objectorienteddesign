//implement states design pattern here
public class BattleShip 
{
    public static void main(String[] args) 
    {
    		PlayerData p1 = new PlayerData("Player 1") ;
    		PlayerData p2 = new PlayerData("Player 2") ;
    		
        PlayerScreen player1 = new PlayerScreen("Player1", true) ;
        BattleShipController bc = new BattleShipController(p1, p2, player1, player1);
    }
    
  
}