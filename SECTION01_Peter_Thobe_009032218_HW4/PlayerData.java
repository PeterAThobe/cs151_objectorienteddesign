/**
 * PlayerData class
 * Model class for playersData
 * @author pthobe4
 */
//need to fix methods for checking hits /how attack grid is implemented

// model 
public class PlayerData 
{
	private final int BOARD_DIMENSION = 10;
	private Boolean attackGrid[][]; // fix access specifier to private
	private String tag ;
	private Ship[] ships = new Ship[5]	;
	private boolean ready = false;
	private int numShips;
	private boolean lost;
	
	PlayerData(String playerTag)
	{
		this.tag = playerTag	;
		this.ships[0] = new Ship(3);
		this.ships[1] = new Ship(3);
		this.ships[2] = new Ship(3);
		this.ships[3] = new Ship(3);
		this.ships[4] = new Ship(3);
		this.lost = false;
		this.numShips = 0;
		
		this.attackGrid = new Boolean[BOARD_DIMENSION][BOARD_DIMENSION];
		
		// Set all player attacked positions to null
		// null = no attack / true = hit/ false = miss
		for(int i = 0 ; i < BOARD_DIMENSION ; i++)
		{
			for(int j = 0 ; j < BOARD_DIMENSION ; j++)
			{
				this.attackGrid[i][j] = null ;
			}
		}
	}
	
	public Ship[] getShips()
	{
		return this.ships ;
	}
	
	public Boolean[][] getAttacked()
	{
		return this.attackGrid;
	}
	
	public void setAttacked(int row, int col, boolean hit)
	{
		this.attackGrid[row][col] = hit;
	}
	public void setShipPosition(int row, int col)
	{
		int pos = this.getNumShips() ;
		if(pos < this.ships.length)
		{
			if(!checkCollision(row, col,(this.ships[pos].getShipSize() - 1) )) 
			{
				if( !this.ships[pos].getShipSet() )
				{ 
					 this.ships[pos].setStart(row, col);
				}
				if(this.numShips < this.ships.length)
				{
					this.numShips++;
				}
			}
		}
	}
	
	public boolean checkGameOver()
	{
		for(int i = 0 ; i < ships.length ; i++)
		{
			if(!this.ships[i].checkSunk())
			{
				return false;
			}
		}
		return true;
	}

	public String getTag() 
	{
		return this.tag ;
	}
	
	public int getActiveShips() 
	{
		int num = 0;
		for(int i = 0 ; i < ships.length ; i++)
		{
			if(!ships[i].checkSunk())
			{
				num++;
			}
		}
		return num	;
	}
	
	public int getNumShips()
	{
		return this.numShips;
	}
	public int getMaxShips()
	{
		return this.ships.length;
	}	
	public void checkReady()
	{
		for(int i = 0 ; i < ships.length ; i++)
		{
			if(!ships[i].getShipSet())	
			{
				this.ready=false;
				break;
			}
			this.ready=true;
		}
	}
	public boolean getReady()
	{
		return this.ready;
	}
	
	public boolean attackShip(int row , int col ) 
	{
		for(int i = 0 ; i < this.ships.length ; i++)
		{
			if(this.ships[i].checkCollision(row, col))
			{
				if(!this.ships[i].checkSunk())
				{
					this.ships[i].markHit();
					return true;
				}
			}
		}
		return false;	
	}
	
	private boolean checkAttackGridSpace(int row, int col)
	{
		if(attackGrid[row][col] == null)
		{
			return false;
		}
		return true;
	}
	
	
//	public boolean checkCollision(int row, int col)// ensure ships do no overlap
//	{
//		for(int i = 0 ; i < this.ships.length ; i++)
//		{
//			if(ships[i].getShipSet())
//			{
//				if(this.ships[i].checkCollision(row, col))
//				{
//					return true;
//				}
//			}
//		}
//		return false; // placement ok
//	}
	
	public boolean checkCollision(int row, int col, int s)// ensure ships do no overlap
	{
		for(int i = 0 ; i < this.ships.length ; i++)
		{
			if(ships[i].getShipSet())
			{
				if(this.ships[i].checkCollision(row, col,s))
				{
					return true;
				}
			}
		}
		return false; // placement ok
	}

	public boolean checkLost()
	{
		int s=0;
		for(int i = 0 ; i < this.ships.length; i++)
		{
			if(ships[i].checkSunk())
			{
				s++;
			}
		}
		if(s >= this.ships.length)
		{
			this.lost = true;
		}
		return this.lost;
	}
	
	
	
}
