
public class P2Turn implements GameState 
{
	BattleShipController controller;
	
	P2Turn(BattleShipController c)
	{
		this.controller = c;
	}
	
	@Override
	public void placeOwnShip() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void AttackEnemyShip() 
	{
		
		
	}



	@Override
	public void swapPlayer() {
		if(!this.controller.checkForWinner())
		{
			if(this.controller.getAttackPositionSelected())
			{
				this.controller.setAttackPositionSelectedFalse();
				this.controller.switchPlayer();
				this.controller.swapView();
				this.controller.setGameState(this.controller.getP1Turn());
				this.controller.updateStatus();
			}
		}
		else
		{
			this.endGame();
		}
	}


	@Override
	public void endGame() 
	{
		this.controller.setGameState(this.controller.getEndGame());
		
	}
	@Override
	public String toString()
	{
		return "P2Turn" ;
	}
	
}
