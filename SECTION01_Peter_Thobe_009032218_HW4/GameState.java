
public interface GameState 
{
	public void placeOwnShip();
	public void AttackEnemyShip();
	public void swapPlayer();
	public void endGame();
	public String toString();
}
