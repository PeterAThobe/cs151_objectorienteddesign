
public class Ship
{
	// need to be able to toggle vertical
	private final int BOARD_DIMENSION = 10; // Board Dimension 
	
	private int[] start	;	//start position of ship (0 = row/1=col)
	private int[] end;	//end position of ship (0 = row/1=col)
	private boolean vertical;		//true = vertical, false = horizontal
	private boolean sunk	;		//true = sunk, false = still active
	private int hits; //Hits used to determine if sunk
	private int size; //size of ship
	private boolean shipSet; // used to tell that ship is not yet set 
	
	//Constructor
	/**
	 * 
	 * @param s
	 */
	Ship(int s)
	{
		this.size = s	;		// sets the size(num cells in length) of ship
		this.vertical = false	; //defaults to a horizontal orientation
		this.start = new int[2]	; // start set by the player
		this.end = new int[2]	; // end determined by orientation and size
		this.sunk = false  	;
		this.shipSet = false	;
	}
	
	
	// getters and setters
	/**
	 * 
	 * @return
	 */
	public int[] getStart()
	{
		return this.start;
	}
	/**
	 * 
	 * @param row
	 * @param col
	 */
	public void setStart(int row, int col)
	{
		
		this.start[0] = row ; // start row
		this.start[1] = col ; // start column
		this.setEnd(row , col); // determines the end point 
		this.setShipSetTrue();
	}
	// set and get for the end points
	/**
	 * 
	 * @return
	 */
	public int[] getEnd()
	{
		return this.end;
	}
	/*
	 * Sets the end point of the ship based off selected start
	 * shifts the ship as necessary to fit inside the dimensions of the board
	 * based on user selection from grid
	 */
	/**
	 * 
	 * @param row
	 * @param col
	 */
	private void setEnd(int row, int col)
	{
		if(this.vertical == true)
		{
			if( row > (BOARD_DIMENSION - this.size) )
			{
				this.setStart(BOARD_DIMENSION - this.size, col);
				this.end[0] = BOARD_DIMENSION	;
				this.end[1] = col	;
			}
			else
			{
				this.end[0] = row + this.size ;
				this.end[1] = col;
			}
		}
		else//horizontal ship orientation
		{
			if( col > (BOARD_DIMENSION - this.size) )
			{
				this.setStart(row, BOARD_DIMENSION - this.size);
				this.end[0] = row	; //
				this.end[1] = BOARD_DIMENSION	;
				
			}
			else
			{
				this.end[0] = row  ;
				this.end[1] = col + this.size;
			}
		}
	}
	/**
	 * changes ships orientation between horizontal and 
	 * vertical
	 */
	public void changeOrientation()
	{
		this.vertical = !this.vertical ;
		setEnd(this.start[0], this.start[1]) ;
	}
	
	/**
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	public Boolean markHit()
	{	
				this.hits++;
				checkSunk();
				return true ;	
	}

	
	/**
	 * 
	 * @return
	 */
	public boolean checkSunk()
	{
		if(this.hits >= this.size && !this.sunk) 
		{ 
			setSunk(); 
		}
		return sunk ;
	}
	/**
	 * 
	 */
	private void setSunk()
	{
		this.sunk = true;
	}
	/**
	 * 
	 * @return
	 */
	public boolean getShipSet()
	{
		return this.shipSet;
	}
	/**
	 * 
	 */
	private void setShipSetTrue()
	{
		this.shipSet = true	;
	}
	
	public boolean getShipOrientation()
	{
		return this.vertical ;
	}
	public int getShipSize()
	{
		return this.size ;
	}

	/**
	 * checks for ship overlapping
	 * @param row row of placement in game
	 * @param col col of placement in game
	 * @return true = ship already in this location, false otherwise
	 */
	public boolean checkCollision(int row, int col)
	{
		if( row >= this.start[0] && row <= this.end[0] ) //row collision detected
		{
			if(col>= this.start[1] && col < this.end[1]) // col collision detected
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean checkCollision(int row, int col, int s)
	{
		int [] shipStart = new int[2];
		int [] shipEnd = new int[2];
		
		shipStart[0] = row;
		shipStart[1] = col;
		shipEnd[0] = row ;
		shipEnd[1] = col + s;
		
		//check if the start point of the ship collides
		if( shipStart[0] >= this.start[0] && shipStart[0] <= this.end[0] ) //row collision detected
		{
			if(shipStart[1]>= this.start[1] && shipStart[1] < this.end[1]) // col collision detected
			{
				return true;
			}
		}
		//check if the end point of the ship collides
		if( shipEnd[0] >= this.start[0] && shipEnd[0] <= this.end[0] ) //row collision detected
		{
			if(shipEnd[1]>= this.start[1] && shipEnd[1] < this.end[1]) // col collision detected
			{
				return true;
			}
		}
		
		return false;
	}
	
}
