import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;

//View
// fix so that panels are accessible 

public class PlayerScreen extends JFrame 
{
	// main elements
	private SelfGrid self;
	private AttackGrid attack;
	private boolean attackable;
	
	//status area
	private JLabel playerTag;
	private JLabel ownShipsRemaining;
	private JLabel enemyShipsSunk;
	private JLabel currentState;
	private JPanel status;
	private JButton next;
	
    public PlayerScreen(String name, boolean show) 
    {
    		
        super("Battle Ship") ;
        this.attackable = false;
        this.setLayout(new BorderLayout()) ;
        
        this.self = new SelfGrid(name);
        this.attack = new AttackGrid(name) ;
        this.add(this.self, BorderLayout.WEST);
        this.add(attack, BorderLayout.EAST);
        this.playerTag = new JLabel(name);
        this.add(playerTag, BorderLayout.NORTH);
        
        //setup of status area
        status = new JPanel();
        status.setLayout(new FlowLayout(FlowLayout.CENTER, 30, 10));
        ownShipsRemaining = new JLabel("Ships Remaining: 5" );
        enemyShipsSunk = new JLabel("Enemy sunk: 0");
        currentState = new JLabel("Active State: Player1setup");
        
        Font font = new Font("Ariel", Font.BOLD, 14);
        
        ownShipsRemaining.setFont(font) ;
        enemyShipsSunk.setFont(font) ;
        currentState.setFont(font) ;
        
        playerTag.setFont(new Font("Ariel", Font.BOLD, 18));
        
        status.add(ownShipsRemaining);
        status.add(enemyShipsSunk);
        status.add(currentState);
        
        this.add(status, BorderLayout.SOUTH);
        
        next = new JButton("next");
        this.add(next, BorderLayout.CENTER);
        this.pack();
        this.setVisible(show);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public boolean getAttackable()
    {
    		return this.attackable;
    }
    
    
    public void addSelfGridMouseListener(int i,MouseListener m)
    {
    		self.addMListener(i, m) ;
    }
    public void addAttackGridListener(int i, MouseListener m)
    {
    		attack.addMListener(i, m) ;
    }
    public void updateSelfGrid(Ship[] ships)
    {
    		self.drawBoats(ships);
    }
    
    public void updateAttackGrid(Boolean[][] attackBoard)
    {
    		attack.drawAttackBoard(attackBoard) ;
    }
    
    public int[] getPanelPosition(int i)
    {
    		return self.getPosition(i) ;
    }

    public void updatePlayerTag(String pt)
    {
    		this.playerTag.setText(pt) ;
    }
    public void updateEnemyShips(int n)
    {
    		this.enemyShipsSunk.setText("Enemy Sunk: " + n);
    }
    public void updateShipsRemaining(int n)
    {
    		this.ownShipsRemaining.setText("Ships Remaining: " + n);
    }
    public void updateCurrentState(String state)
    {
    		this.currentState.setText("Current State: " + state);
    }

    public void setButtonListener(ActionListener a)
    {
    		this.next.addActionListener(a);
    		this.revalidate();
    		this.repaint();
    }
    
    public void repaintSelfGrid()
    {
    		this.self.repaint();
    }

    public void displayWinner(String winnerName)
    {
    		this.playerTag.setText("THE WINNER IS " + winnerName.toUpperCase());
    }
}