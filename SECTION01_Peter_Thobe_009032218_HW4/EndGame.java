
public class EndGame implements GameState
{
	BattleShipController controller;
	
	EndGame(BattleShipController c)
	{
		this.controller = c;
	}
	@Override
	public void placeOwnShip() 
	{

		
	}

	@Override
	public void AttackEnemyShip() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void swapPlayer() 
	{
		this.endGame();
		
	}

	@Override
	public void endGame() 
	{
		this.controller.updateStatus();
		this.controller.displayWinner();
	}
	
	@Override
	public String toString()
	{
		return "GAME_OVER";
	}

}
