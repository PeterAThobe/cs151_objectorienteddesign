import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;

/**
Represents the player's own grid
*/
public class AttackGrid extends BattleGrid 
{
	private final int DIMENSION = 30;
    public AttackGrid(String name) 
    {
        super();
    }

    @Override
    protected JPanel getCell()
    {
        JPanel panel = new JPanel();
        panel.setBackground(Color.white);
        panel.setBorder(BorderFactory.createLineBorder(Color.black, 5));
        panel.setPreferredSize(new Dimension(DIMENSION, DIMENSION)); // for demo purposes only
        
        return panel;
    }
    
}