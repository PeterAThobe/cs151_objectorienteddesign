import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class WhiteBoard extends JFrame
{
	public WhiteBoard()
	{
		super();
		this.setLayout(new BorderLayout()) ;
		Canvas panel = new Canvas();
		this.add(panel, BorderLayout.CENTER); //Canvas added to the center of frame
		Controls c = new Controls(panel,this);
		panel.setControl(c);
		this.add(c, BorderLayout.WEST);
		//add the controls to the west of the frame
		
		
		
		
		//Setting frame properties
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.pack(); //ask why this is not creating to fit the size of both panels


		
	}
	
	//Main program
	public static void main(String [] args)
	{	
		WhiteBoard main = new WhiteBoard() ;
		WhiteBoard client = new WhiteBoard();
	}
}
