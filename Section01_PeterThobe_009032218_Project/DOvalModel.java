import java.awt.Color;

public class DOvalModel extends DShapeModel
{
	public DOvalModel()
	{
		super();
		setX(50);
	}
	
	public void setID(int i)
	{
		super.setID(i);
		notifyObservers() ;
	}
	public void setColor(Color c)
	{
		super.setColor(c);
	}
}
