import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;

public class DText extends DShape
{
	private Font font;
	private String prevFont ;
	private int prevHeight ;
	private boolean redraw ;
	
	public DText(Canvas canvas, Controls control)
	{
		super(new DTextModel(), canvas, control);
		font = null ;
		redraw = true;
		prevFont = "" ;
		prevHeight = -1 ;
		//set the initial height and
		model.setX(100);
		model.setY(100);
		model.setWidth(100) ;
		model.notifyObservers();
	}
	
	public String getText()
	{
		DTextModel text = (DTextModel)this.model ;
		return text.getText() ;
	}
	public void setText(String s)
	{
		DTextModel text = (DTextModel)this.model ;
		text.setText(s) ;
	}
	public String getFontName()
	{
		DTextModel text = (DTextModel)this.model ;
		return text.getFontName() ;
	}
	public void setFontName(String f)
	{
		DTextModel text = (DTextModel)this.model ;
		if(f.equals(getFontName()))
		{
			return;
		}
		text.setFontName(f) ;
	}
	
	
	@Override
	public void draw(Graphics g) 
	{
		DTextModel text = (DTextModel)model;
		Graphics2D g2 = (Graphics2D)(g) ;
		g2.setColor(text.getColor());
		font = new Font(text.getFontName(), Font.BOLD,model.getHeight());
		//font = setFont(g2);
		Shape clip = g2.getClip();
		g2.setClip(clip.getBounds2D().createIntersection(text.getBounds()));
		g2.setFont(font) ;
		g2.drawString(text.getText(), text.getX(), text.getY()+text.getHeight()-5);
		if(drawKnobs)
		{
			drawKnobs(g);
		}
		g2.setClip(clip);
	}
	

	@Override
	public String getClassName() 
	{
		return "Text" ;
	}
	

}
