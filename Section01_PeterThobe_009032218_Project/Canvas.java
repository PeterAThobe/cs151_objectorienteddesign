import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JPanel;



public class Canvas extends JPanel implements ModelListener 
{
	private static final int DIMENSION = 400;
	private ArrayList<DShape> shapes;
	private DShape selected;
	
	private Points move;
	private Points anchor;
	private Controls control;
	
	public Canvas()
	{	
		super();
		this.setPreferredSize(new Dimension(DIMENSION,DIMENSION));
		this.setBackground(Color.white);
		this.setVisible(true);
		shapes= new ArrayList<DShape>();
		selected = null ;
		// MouseListeners
		this.addMouseListener(new MouseAdapter() 
		{
			public void mousePressed(MouseEvent e)
			{
				if(control != null && control.notClient())
				{
					hideKnobs() ;
					determineSelected(new Points(e.getX(),e.getY()));
					shapesCallnotify();
				}
			}
		});
		
		this.addMouseMotionListener(new MouseMotionListener() 
		{

			@Override
			public void mouseDragged(MouseEvent e) 
			{
				if(control != null && control.notClient())
				{
					if(selected != null )
					{
							if(selected.getMoveable())
							{
								
									selected.resize(new Points(e.getX(),e.getY()), anchor);
							}
							else//if not moveable
							{
								if(!(selected.getModel() instanceof DLineModel))
								{
									int radiusW = selected.getModel().getWidth()/2 ;
									int radiusH = selected.getModel().getHeight()/2 ;
									selected.getModel().setPosition(e.getX() - radiusW , e.getY() - radiusH) ;
								}
								else if(selected.getModel() instanceof DLineModel)
								{
									DLineModel line = (DLineModel)selected.getModel() ;
									Points center = line.getCenterPT() ;
									line.setPosition(e.getX() - center.getX(), e.getY() - center.getY()) ;
									shapesCallnotify();
								}
							}
					}
						selected.getModel().notifyObservers();
						shapesCallnotify();
				}
				
			}

			@Override
			public void mouseMoved(MouseEvent e) 
			{
				// TODO Auto-generated method stub
			}
			
		});
		

		
		
		
		//end of main program 
		
	}
	
	
	
	
	
	//helper methods
	public void paint() 
	{
		
	}
//old method no longer used
//	public void addShape(DShapeModel shape, Controls c)
//	{
//		if( selected!= null)
//		{
//			selected.setKnobsVisible(false) ;
//			repaint();
//		}
//		if(shape instanceof DRectangleModel)
//		{
//			shapes.add(new DRectangle(this,c)) ;
//		}
//		if(shape instanceof DOvalModel)
//		{
//			shapes.add(new DOval(this,c)) ;
//		}
//		if(shape instanceof DLineModel)
//		{
//			shapes.add(new DLine(this,c)) ;
//		}
//		if(shape instanceof DTextModel)
//		{
//			shapes.add(new DText(this,c)) ;
//		}
//		setSelected(null) ;
//		if(control!=null && !control.notServer())
//		{
//			System.out.println("Server Model Id" + shape.getID());
//			control.doSend(0, shape);
//		}
//		
//		repaint();
//	}
	public void newaddShape(DShapeModel m, Controls c)
	{
		DShape shape = null;
		if( selected!= null)
		{
			selected.setKnobsVisible(false) ;
			repaint();
		}
		if(m instanceof DRectangleModel)
		{
			shape = new DRectangle(this,c) ;
		}
		if(m instanceof DOvalModel)
		{
			shape = new DOval(this,c) ;
		}
		if(m instanceof DLineModel)
		{
			shape = new DLine(this,c) ;
		}
		if(m instanceof DTextModel)
		{
			shape = new DText(this,c) ;
		}
		setSelected(null) ;
		if(control !=null && !control.notServer())
		{
			control.doSend(0, shape.getModel());
		}
		shapes.add(shape) ;
		shape.getModel().notifyObservers();
		repaint();
	}
	public void ClientAddShape(DShapeModel m, Controls c)
	{
		if(selected!=null)
		{
			selected.setKnobsVisible(false);
			repaint();
		}
		DShape shape = null;
		if(m instanceof DRectangleModel)
		{
			shape = new DRectangle(this,c) ;
			shape.getModel().setX(m.getX());
			shape.getModel().setY(m.getY());
			shape.getModel().setWidth(m.getWidth());
			shape.getModel().setHeight(m.getHeight());
			shape.getModel().setColor(m.getColor());
			shape.getModel().setID(m.getID());
		}
		if(m instanceof DOvalModel)
		{
			shape = new DOval(this,c) ;
			shape.getModel().setX(m.getX());
			shape.getModel().setY(m.getY());
			shape.getModel().setWidth(m.getWidth());
			shape.getModel().setHeight(m.getHeight());
			shape.getModel().setColor(m.getColor());
			shape.getModel().setID(m.getID());
		}
		if(m instanceof DLineModel)
		{
			shape = new DLine(this,c) ;
			shape.getModel().setX(m.getX());
			shape.getModel().setY(m.getY());
			shape.getModel().setWidth(m.getWidth());
			shape.getModel().setHeight(m.getHeight());
			shape.getModel().setColor(m.getColor());
			shape.getModel().setID(m.getID());
			((DLineModel) shape.getModel()).setP1(((DLineModel) m).getP1()) ;
			((DLineModel) shape.getModel()).setP2(((DLineModel) m).getP2()) ;
		}
		if(m instanceof DTextModel)
		{
			shape = new DText(this,c) ;
			shape.getModel().setX(m.getX());
			shape.getModel().setY(m.getY());
			((DTextModel) shape.getModel()).setWidth(((DTextModel) m).getWidth()) ;
			shape.getModel().setHeight(m.getHeight());
			shape.getModel().setColor(m.getColor());
			shape.getModel().setID(m.getID());
			((DTextModel) shape.getModel()).setFontName(((DTextModel) m).getFontName()) ;
			((DTextModel) shape.getModel()).setText(((DTextModel) m).getText()) ;
		}
		shapes.add(shape);
		shape.getModel().notifyObservers();
		repaint();
	}
	public void paintComponent(Graphics g)//fix
	{
		super.paintComponent(g) ;
		for(DShape d: this.shapes)
		{
			d.draw(g);
		}
	}
	
	public void setSelected(DShape shape)
	{
		this.selected = shape ;
	}

	public ArrayList<DShape> getShapes(){ return this.shapes ; }

	@Override//implementation of modelistener interface(observer)
	public void modelChanged(DShapeModel model) 
	{
		if(!control.notServer())
		{
			control.doSend(4, model);
		}
		repaint(); //repaint the entire canvas
	}
	
	public DShape getSelected()
	{
		return this.selected ;
	}
	
	public void hideKnobs()
	{
		if(this.selected != null)
		{
			this.selected.setKnobsVisible(false) ;
			this.selected.setMoveable(false);
			this.selected.getModel().notifyObservers() ;
		}
	}
	
	public void determineSelected(Points p)
	{

		move = null ;
		anchor = null ;
		
		for(DShape m : shapes)
		{
			if (m.getModel().checkBounds(p.getX(), p.getY()))
			{
				if(m != selected || selected == null)
				{
					selected = m ;
				}
			}
		}
		if(selected!=null)
		{
			selected.setKnobsVisible(true);
			if(!control.notServer())
			{
				control.doSend(4, selected.getModel());
			}
		}
		if( selected.checkKnobClicked(p.getX(), p.getY()) ) 
		{
			move = selected.getCLickedPoint(p) ;
			anchor = selected.getAnchor(move) ;
		}
		
		selected.getModel().notifyObservers();
	}
	
	public void clientUpdate(DShapeModel m)
	{
		if(!control.notClient())//means it is in client mode
		{
			//selected = null ; //reset selected
			hideKnobs();
			selected = getShape(m);
			selected.setKnobsVisible(true);
			selected.getModel().setColor(m.getColor());	
			
			if(selected.getModel() instanceof DTextModel)
			{
				((DTextModel)selected.getModel()).setText(((DTextModel) m).getText());
				((DTextModel)selected.getModel()).setFontName(((DTextModel) m).getFontName());
			}
			selected.getModel().setX(m.getX());
			selected.getModel().setY(m.getY());
			selected.getModel().setWidth(m.getWidth());
			selected.getModel().setHeight(m.getHeight());
			
			if(selected.getModel() instanceof DLineModel)
			{
				((DLineModel)selected.getModel()).setP1(((DLineModel)m).getP1());
				((DLineModel)selected.getModel()).setP2(((DLineModel)m).getP2());
				
			}
			
			
			selected.getModel().notifyObservers();
		}
		repaint();
	}
	
	public void deleteItem()
	{
		if(selected != null)
		{
			shapes.remove(selected) ;
		}
		if(!control.notServer())
		{
			control.doSend(1, selected.getModel());
		}
		this.shapesCallnotify();
		repaint() ;	
	}
	public void deleteShape(DShape shape)
	{
		shapes.remove(shape) ;
		repaint();
		this.shapesCallnotify();
	}
	public void moveFront()
	{
		if(selected != null)
		{
			DShape temp = selected;
			shapes.remove(selected) ;
			shapes.add(temp);
			this.selected = temp;
			repaint();
		}
		if(!control.notServer())
		{
			control.doSend(2, selected.getModel());
		}
		this.shapesCallnotify();
	}
	public void moveBack()
	{
		if(selected != null)
		{
			DShape temp = selected;
			shapes.remove(selected) ;
			shapes.add(0, temp);
			selected = temp;
			repaint();
		}
		if(!control.notServer())
		{
			control.doSend(2, selected.getModel());
		}
		this.shapesCallnotify();
	}
	
	public void toFront(DShape shape)
	{
		DShape temp = shape;
		shapes.remove(shape) ;
		shapes.add(temp);
		this.selected = temp;
		repaint();
		this.shapesCallnotify();
	}
	public void toBack(DShape shape)
	{
		
			DShape temp = shape;
			shapes.remove(shape) ;
			shapes.add(0, temp);
			selected = temp;
			repaint();
			
			this.shapesCallnotify();
	}
	
	public DShape getShape(DShapeModel model)
	{
		DShape temp = null;
		for(DShape s: getShapes())
		{
			if(s.getModel().compare(model) )
			{
				temp = s ;
				break;
			}
		}
		return temp;
	}
	public void setControl(Controls c)
	{
		this.control = c ;
	}
	
	public void setColor(Color c)
	{
		if(selected!=null)
		{
			selected.getModel().setColor(c);
			if(!control.notServer())
			{
				control.doSend(4, selected.getModel());
			}
		}
		
	}
	
	public void TextUpdate(String s)
	{
		if(selected.getModel() instanceof DTextModel)
		{
			((DTextModel)selected.getModel()).setText(s) ;
			if(!control.notServer())
			{
				control.doSend(4,selected.getModel()) ;
			}
		}
		this.shapesCallnotify();
	}
	public void fontUpdate(String s)
	{
		if(selected.getModel() instanceof DTextModel)
		{
			((DTextModel)selected.getModel()).setFontName(s) ;
			if(!control.notServer())
			{
				control.doSend(4,selected.getModel()) ;
			}
		}
		this.shapesCallnotify();
	}
	
	public void shapesCallnotify()
	{
		if(!shapes.isEmpty())
		{
			for(DShape s: shapes)
			{
				s.getModel().notifyObservers();
			}
		}
	}
	
	
	
}
