import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Comparator;
//Subject part of the Observer Pattern
public abstract class DShapeModel implements Observable
{
	private int x;
	private int y;
	private int width;
	private int height;
	Color color;
	protected ArrayList<ModelListener> modelListeners ; //list of Observers
	private int ID ;
	private static int COUNTER;
	public DShapeModel()
	{
		this.ID = this.COUNTER ;
		//System.out.println(ID);
		this.x = 0;
		this.y = 0;
		this.width = 30 ;
		this.height = 30 ;
		this.color = Color.GRAY ;
		this.modelListeners = new ArrayList<ModelListener>() ;
		notifyObservers();
		this.COUNTER++ ;
	}
	
	public int getX() { return this.x; }
	public int getY() { return this.y; }
	public int getWidth() {return this.width; }
	public int getHeight() {return this.height; }
	public Color getColor() {return this.color ; }
	public void setPosition(int x,int y)
	{
		this.setX(x);
		this.setY(y);
		this.notifyObservers() ;
	}
	public void setX(int x) {this.x = x ; }
	public void setY(int y) {this.y = y ; }
	public void setWidth(int w) {this.width = w ; }
	public void setHeight(int h) {this.height = h ; }
	public void setColor(Color c) 
	{ 
		this.color = c ; 
		notifyObservers();
	}
	
	public boolean checkBounds(int x , int y) //may not need 
	{
		int maxX = getX() + getWidth() ;
		int maxY = getY() + getHeight() ;
		if(x >= getX()  && x <= maxX)
		{
			if(y >= getY() && y <= maxY)
			{
				return true;
			}
		}
		return false;
	}
	
	//implement Observable methods
	public void add(ModelListener ml)
	{
		this.modelListeners.add(ml) ;
	}
	public void remove(ModelListener ml)
	{
		this.modelListeners.remove(ml) ;
	}
	public void notifyObservers()
	{
		for(ModelListener ml : modelListeners)
		{
			ml.modelChanged(this) ; //CHECK THIS IMPLEMENTATION
		}
	}
	//end of Observable method implementation
	
	public void resize(Points a, Points m) //additional width and height to add
	{
		int x = (a.getX() < m.getX() ? a.getX():m.getX());
		int y = (a.getY() < m.getY() ? a.getY():m.getY());
		int w = Math.abs(a.getX() - m.getX()) ;
		int h = Math.abs(a.getY() - m.getY()) ;
		
		this.x = x ;
		this.y = y ;
		this.width = w ;
		this.height = h ;
		notifyObservers() ;
	}
	public Rectangle getBounds()
	{
		return new Rectangle(this.x, this.y, this.width, this.height) ;
	}
	public int getID()
	{
		return this.ID;
	}
	public boolean compare(DShapeModel m)
	{
		if(getID() != m.getID())
		{
			return false ;
		}
		return true;
	}
	
	public void setID(int i)
	{
		this.ID = i;
	}
	
}
