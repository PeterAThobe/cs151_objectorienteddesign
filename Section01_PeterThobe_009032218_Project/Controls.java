import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

import sun.rmi.runtime.NewThreadAction;

import javax.imageio.ImageIO;


public class Controls extends JPanel implements ModelListener
{
	protected static int ID;
	private JPanel addShapePanel ;
	private JPanel colorSelectionPanel;
	private JPanel textArea ;
	private JPanel shapePosition ;
	private JPanel networkingPanel ;
	private JScrollPane scrollable ;
	private JTable table;
	private DefaultTableModel tableModel ;
	private JPanel editSavePane ;
	private Canvas canvas;
	private JLabel currentNetworkingState;
	private JTextField textBox;
	private JComboBox scriptSelect ;
	private String functionalMode ;
	private ArrayList<ObjectOutputStream> outputs;
	
	
	public Controls(Canvas c, WhiteBoard w)
	{
		super();
		outputs = new ArrayList<ObjectOutputStream>();
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)) ;
		this.canvas = c ;
		this.functionalMode = " ";
		// create the add section
		
		addShapePanel = new JPanel();
		addShapePanel.setLayout(new BoxLayout(addShapePanel, BoxLayout.X_AXIS)); //add button horizontally
		addShapePanel.setAlignmentX(addShapePanel.LEFT_ALIGNMENT);
		JLabel add = new JLabel("Add Shape: ") ;
		JButton rect = new JButton("rectangle") ;
		JButton oval = new JButton("oval") ;
		JButton line = new JButton("line") ;
		JButton text = new JButton("text") ;
		
		// add all shapes to the panel
		addShapePanel.add(add) ;
		addShapePanel.add(rect) ;
		addShapePanel.add(oval) ;
		addShapePanel.add(line) ;
		addShapePanel.add(text) ;
		
		this.add( addShapePanel) ;
		
		//color section
		colorSelectionPanel = new JPanel();
		colorSelectionPanel.setLayout(new BoxLayout(colorSelectionPanel, BoxLayout.X_AXIS)) ;
		colorSelectionPanel.setAlignmentX(colorSelectionPanel.LEFT_ALIGNMENT);
		JLabel colorText = new JLabel("Set Shape Color: ");
		JButton colorSelector = new JButton("Pick Color") ;
		colorSelector.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Color newColor = JColorChooser.showDialog(colorSelectionPanel, "New Color Selector", Color.black);
				if(canvas.getSelected() != null)
				{
					//canvas.getSelected().getModel().setColor(newColor) ;
					canvas.setColor(newColor);
				}
			}
		} );
		colorSelectionPanel.add(colorText);
		colorSelectionPanel.add(colorSelector) ;
		
		this.add(colorSelectionPanel);
		
		//Text Selection Pane
		
		JPanel textArea = new JPanel();
		textArea.setLayout(new BoxLayout(textArea, BoxLayout.X_AXIS)) ;
		textArea.setAlignmentX(textArea.LEFT_ALIGNMENT);
			textBox = new JTextField("Enter Text Here") ;
			//JButton scrpitSelect = new JButton("Current Script type") ;//place holder for JComboBox
			textBox.setMaximumSize(new Dimension(300,25));
				//add actionListener to select scriptType
			//Fonts area of the Table
			
			GraphicsEnvironment graphEnv = GraphicsEnvironment.getLocalGraphicsEnvironment() ;
			String [] availableFonts = graphEnv.getAvailableFontFamilyNames() ;
			scriptSelect = new JComboBox(availableFonts);
			scriptSelect.setMaximumSize(new Dimension(200,25));
			setTextfunctionality(false);
			
		textArea.add(new JLabel("Edit Text Field: "));
		textArea.add(textBox) ;
		textArea.add(scriptSelect) ;
		//textArea.add(scrpitSelect) ;
		this.add(textArea) ;
		// Shape Position Pane
		
		
		shapePosition = new JPanel();
		shapePosition.setLayout(new BoxLayout(shapePosition, BoxLayout.X_AXIS));
		shapePosition.setAlignmentX(shapePosition.LEFT_ALIGNMENT);
			JButton toFront = new JButton("Move to front") ;
			JButton toBack = new JButton("Move to back") ;
			JButton delete = new JButton("Delete") ;
			
		shapePosition.add(new JLabel("Edit Shape: ")) ;
		shapePosition.add(toFront) ;
		shapePosition.add(toBack) ;
		shapePosition.add(delete) ;
		
		//edit/save 
		editSavePane = new JPanel();
		editSavePane.setLayout(new BoxLayout(editSavePane, BoxLayout.X_AXIS));
		editSavePane.setAlignmentX(editSavePane.LEFT_ALIGNMENT);
		JButton edit = new JButton("Save Content") ;
		JButton load = new JButton("Load Content") ;
		JButton exportImage = new JButton("Save as PNG") ;
		
		editSavePane.add(new JLabel("Edit/Save/Export: ")) ;
		editSavePane.add(edit);
		editSavePane.add(load);
		editSavePane.add(exportImage);
		
		this.add(editSavePane) ;
		
		this.add(shapePosition) ;
		//networking Section
		networkingPanel = new JPanel();
		networkingPanel.setLayout(new BoxLayout(networkingPanel, BoxLayout.X_AXIS));
		networkingPanel.setAlignmentX(networkingPanel.LEFT_ALIGNMENT);
		
		JButton server = new JButton("Start Server") ;
		JButton client = new JButton("Start Client") ;
		currentNetworkingState = new JLabel("Current Networking State") ;
		
		networkingPanel.add(new JLabel("Server/Client:")) ;
		networkingPanel.add(server) ;
		networkingPanel.add(client) ;
		networkingPanel.add(currentNetworkingState);

		
		this.add(networkingPanel) ;
		
		//table for display of shape Models 
		tableModel = new DefaultTableModel(new String[] {"Shape Type","X","Y","Width","Height"},0) ;
		table = new JTable(tableModel) ;
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS) ;
		scrollable = new JScrollPane(table);
		scrollable .setPreferredSize(new Dimension(300, 200));
		this.add(scrollable) ;
		
		
		// adding actionListener to buttons
		
		rect.addActionListener(new ActionListener() 
		{

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				c.newaddShape(new DRectangleModel(),getOuterClass());
				c.repaint();
			}
			
		});
		
		oval.addActionListener(new ActionListener() 
		{

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				c.newaddShape(new DOvalModel(), getOuterClass());
				c.repaint();
			}
			
		});
		
		line.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				c.newaddShape(new DLineModel(), getOuterClass());
				c.repaint();
			}
			
		});
		
		text.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				c.newaddShape(new DTextModel(), getOuterClass());
				c.repaint();
			}
		});
		
		delete.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				c.deleteItem() ;
			}
		});
		
		toFront.addActionListener(new ActionListener() 
		{

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				c.moveFront();
			}
			
		});
		toBack.addActionListener(new ActionListener() 
		{

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				c.moveBack();
			}
			
		});
		textBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String text = (String)textBox.getText() ;
				DTextModel m = (DTextModel)canvas.getSelected().getModel();
				//m.setText(text);
				canvas.TextUpdate(text);
			}
		});
		scriptSelect.addActionListener(new ActionListener() 
		{
			
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				String text = (String)scriptSelect.getSelectedItem() ;
				DTextModel m = (DTextModel)canvas.getSelected().getModel();
				//m.setFontName(text);
				canvas.fontUpdate(text) ;
				
			}
		});
		
		// server and client action listeners
		server.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				String serverPort = JOptionPane.showInputDialog
						("Enter new server port number(try large number like 56716): ", "56716");
				
				if(serverPort != null)
				{
					client.setEnabled(false);
					server.setEnabled(false);
					currentNetworkingState.setText("Server: " + serverPort);
					functionalMode = "Server";
					ServerAccepter serverAccepter = 
								new ServerAccepter(Integer.parseInt(serverPort.trim())) ;
					serverAccepter.start();
				}
			}
		});
		client.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				
				String clientPort = JOptionPane.showInputDialog
						("Connect to the host:port : ", "127.0.0.1:56716");
				
				if(clientPort != null)
				{
					String[] ipAddressPort = clientPort.split(":") ; 
					server.setEnabled(false);
					client.setEnabled(false);
					currentNetworkingState.setText("Client: "+ clientPort);
					functionalMode = "Client";
					ClientHandler clientH = new ClientHandler(ipAddressPort[0].trim(), 
							Integer.parseInt(ipAddressPort[1].trim()));
					clientH.start();
					disableControls();
					edit.setEnabled(true);
					exportImage.setEnabled(true);
				}
			}
		});
		
		//add save/open/print
		
		
		edit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				
					JFileChooser choice = new JFileChooser();
					int state = choice.showSaveDialog(w);
					
					if(state == JFileChooser.APPROVE_OPTION)
					{
						save(choice.getSelectedFile()) ;
					}
	
			}
		});
		load.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(notClient() && notServer())
				{
					JFileChooser choice = new JFileChooser();
					int state = choice.showOpenDialog(w);
					
					if(state == JFileChooser.APPROVE_OPTION)
					{
						load(choice.getSelectedFile()) ;
					}
					
				}
			}
		});
		
		exportImage.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) 
			{

					JFileChooser choice = new JFileChooser();
					int state = choice.showSaveDialog(w);
					
					if(state == JFileChooser.APPROVE_OPTION)
					{
						printImage(choice.getSelectedFile()) ;
					}

			}
		});
		//end of constructor-------
	}
	public void disableControls()
	{
		for(Component c: this.getComponents())
		{
			
			for(Component d: ((Container) c).getComponents())
			{
				d.setEnabled(false);
			}
		}
	}
	public Controls getOuterClass() //helper method to get reference to outerclass of controls
	{
		return this;
	}
	public void updateShapeList()//need to fix this area causes error on reprint menu
	{
		if(tableModel.getRowCount() > 0)
		{
			this.tableModel.setRowCount(0);
		}
		for(DShape s : canvas.getShapes())
		{
			String[] shape = {s.getClassName(), 
								Integer.toString(s.getModel().getX()),
								Integer.toString(s.getModel().getY()),
								Integer.toString(s.getModel().getWidth()),
								Integer.toString(s.getModel().getHeight()) } ;
			addShapetoList(shape) ;
		}
	}
	
	//add listener add buttons
	public void addShapetoList(String[] s)
	{
		tableModel.addRow(s) ;
	}

	@Override
	public void modelChanged(DShapeModel model) 
	{
		this.updateShapeList(); //update the list of shapes based off this
	}
	
	public void changeText(String s)
	{
		textBox.setText(s);
	}
	public void setFontType(String s)
	{
		scriptSelect.setSelectedItem(s);
	}
	
	public void setTextfunctionality(boolean f)
	{
		textBox.setEnabled(f) ;
		scriptSelect.setEnabled(f) ;
	}
	
	public void setServerMode()
	{
		this.functionalMode = "server" ;
	}
	public void setClientMode() 
	{
		this.functionalMode = "client" ;
	}
	public void setNormalMode()
	{
		this.functionalMode = "normal";
	}
	
	//Server/Client Coding
	

public static class Message 
{
	public static final int ADD = 0 ; 
	public static final int REMOVE = 1 ;
	public static final int FRONT = 2 ;
	public static final int BACK = 3 ;
	public static final int CHANGE = 4;
	
	
	public int command;
	public DShapeModel model;
	
	public Message()
	{
		this.command = -1;
		this.model = null ;
	}
	
	public Message(int c, DShapeModel m)
	{
		this.command = c ; 
		this.model = m ;
	}
	
	public int getCommand()
	{
		return this.command ; 
	}
	public void setCommand(int index)
	{
		this.command = index ;
	}
	public DShapeModel getModel()
	{
		return this.model ;
	}
	public void setModel(DShapeModel m)
	{
		this.model = m;
	}
	
	public String toString()
	{
		String text = " " ;
		switch(command)
		{
			case 0:
				text = "ADD" ;
				break;
			case 1:
				text = "REMOVE" ;
				break;
			case 2:
				text = "FRONT" ;
				break;
			case 3:
				text = "BACK" ;
				break;
			case 4:
				text = "CHANGE" ;
				break;
		}
		return "Message: " + text ;
	}
}

public class ServerAccepter extends Thread
{
	private int serverPortNumber;
	
	public ServerAccepter(int port)
	{
		this.serverPortNumber = port ;
	}
	
	public void run()
	{
		try
		{
			ServerSocket serverSocket = new ServerSocket(serverPortNumber) ;
			while(true) 
			{
				Socket toClient = null;
				toClient = serverSocket.accept() ;
				final ObjectOutputStream out = new ObjectOutputStream(toClient.getOutputStream());
				
				if(!outputs.contains(out))
				{
					Thread worker = new Thread(new Runnable() 
					{
						public void run()
						{
							for(DShape shape: canvas.getShapes())
							{
								try 
								{
									out.writeObject(messageToXML(new Message(Message.ADD, shape.getModel())));
									out.flush() ;
								}
								catch (Exception e)
								{
									e.printStackTrace();
								}
							}
						}
					});
					worker.start() ;
				}
				addOutput(out);
			}
		}catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
}

//ClientHandler

public class ClientHandler extends Thread
{
	private String name;
	private int portNum;
	
	public ClientHandler(String name, int port)
	{
		this.name = name;
		this.portNum = port;
	}
	
	public void run()
	{
		try
		{
			Socket server = new Socket(this.name,this.portNum) ;
			ObjectInputStream in = new ObjectInputStream(server.getInputStream()) ;
			boolean stillValid = true;
			while(stillValid)
			{
				String xmlString = (String)in.readObject();
				XMLDecoder decoder = new XMLDecoder(new ByteArrayInputStream(xmlString.getBytes()));
				Message m = (Message) decoder.readObject() ;
				//Message m = XMLToMessage(xmlString) ;
				convertMessage(m);
				decoder.close();
			}
		}catch(Exception e)
		{
			//do nothing
		}
	}
}

public Message XMLToMessage(String input)
{
	XMLDecoder decoder = new XMLDecoder(new ByteArrayInputStream(input.getBytes())) ;
	Message m = (Message)decoder.readObject();
	decoder.close();
	return m;
	
}
public String messageToXML(Message m)
{
	OutputStream memoryStream = new ByteArrayOutputStream();
	XMLEncoder encoder = new XMLEncoder(memoryStream);
	encoder.writeObject(m) ;
	encoder.close();
	return memoryStream.toString() ;
}
	
	public synchronized void addOutput(ObjectOutputStream out)
	{
		this.outputs.add(out) ;
	}
	
	
	public void convertMessage(Message m)
	{
		SwingUtilities.invokeLater(new Runnable() 
		{

			@Override
			public void run() 
			{
				DShape shape = canvas.getShape(m.getModel()) ;
					switch(m.getCommand())
					{
						case 0:
							if(shape == null)
								canvas.ClientAddShape(m.getModel(), getOuterClass());
							break;
						case 1:
							if(shape != null)
								canvas.deleteShape(shape);
							break;
						case 2:
							if(shape != null)
								canvas.toBack(shape);
							break;
						case 3:
							if(shape != null)
								canvas.toFront(shape);
							break;
						case 4:
							if(shape != null)
								canvas.clientUpdate(m.getModel());
							break;
						case 5:
							if(shape!=null)
								canvas.setColor(shape.getModel().getColor());
					}
			}
			
		});
		
	}
	
	public boolean notClient()
	{
		if(functionalMode.toLowerCase().equals("client"))
		{
			return false;
		}
		return true;
	}
	public boolean notServer()
	{
		if(functionalMode.toLowerCase().equals("server"))
		{
			return false;
		}
		return true;
	}
	
	public void doSend(int command, DShapeModel model)
	{
		Message message = new Message(command, model) ;
		sendRemote(message);
	}
	public void sendRemote(Message message)
	{
		String xmlString = messageToXML(message) ;
		Iterator<ObjectOutputStream> it = outputs.iterator() ;
		while(it.hasNext())
		{
			ObjectOutputStream out = it.next();
			try
			{
				out.writeObject(xmlString);
				out.flush();
			}catch(Exception ex)
			{
				ex.printStackTrace();
				it.remove();
			}
		}
	}
	public void incID()
	{
		this.ID++ ;
	}
	
	public void save(File f)
	{
		try 
		{
			XMLEncoder out = new XMLEncoder(new FileOutputStream(f));
			DShapeModel[] models = canvas.getShapes().toArray(new DShapeModel[0]);
			out.writeObject(models);
			out.close();
			
		} catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void load(File f)
	{
		for(DShape s: canvas.getShapes())
		{
			canvas.deleteShape(s);
		}
		
		try {
			XMLDecoder input = new XMLDecoder(new FileInputStream(f)) ;
			DShapeModel[] models = (DShapeModel[])input.readObject() ;
			input.close();
			
			for(DShapeModel m: models)
			{
				canvas.newaddShape(m, this);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void printImage(File f)
	{
		BufferedImage image = (BufferedImage) canvas.createImage(canvas.getWidth(),
				canvas.getHeight());
		Graphics2D g = (Graphics2D) image.getGraphics() ;
		canvas.paint(g);
		try {
			ImageIO.write(image, "PNG", f) ;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
