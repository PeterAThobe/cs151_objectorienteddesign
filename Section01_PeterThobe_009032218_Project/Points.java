
public class Points 
{
	private int x ;
	private int y ;
	
	public Points(int x, int y)
	{
		this.x = x ;
		this.y = y ;
	}
	
	public int getX() {return this.x ;}
	public int getY() {return this.y ;}
	public void setX(int x) {this.x = x ;}
	public void setY(int y) {this.y = y ;}
	
	public boolean checkCollision(int r, Points p)
	{
		if(p.getX() >= this.x && p.getX() <= this.x+r)
		{
			if(p.getY() >= this.y && p.getY() <= this.y+r )
			{
				return true;
			}
		}
		return false;
	}
}
