import java.awt.Color;

public class DLineModel extends DShapeModel 
{
	private Points p1 ;
	private Points p2 ;
	
	public DLineModel()
	{
		super();
		this.p1 = new Points(50,50) ;
		this.p2 = new Points(80,80) ;
		//SETTING THE BOUNDING BOX UPPER LEFT TO P1
		this.setX(p1.getX()) ; 
		this.setY(p1.getY()) ;
		//Setting the width and height based off the height and 
		this.setWidth( Math.abs(p2.getX() - p1.getX()) );
		this.setHeight( Math.abs(p2.getY() - p1.getY()) );
		notifyObservers();
	}
	
	public Points getP1() {return this.p1 ;}
	public Points getP2() {return this.p2 ;}
	
	public void setP1(Points p) 
	{
		this.setX(p.getX());
		this.setY(p.getY());
		this.p1 = p ;
		notifyObservers() ;
	}
	public void setP2(Points p) 
	{
		this.p2 = p ;
		notifyObservers() ;
	}
	private void setWidth()
	{
		this.setWidth( Math.abs(p2.getX() - p1.getX()));
		notifyObservers();
	}
	private void setHeight()
	{
		this.setHeight( Math.abs(p2.getY() - p1.getY()));
		notifyObservers();
	}
	
	@Override
	public boolean checkBounds(int x, int y) // mightneed to check this 
	{
		int tlX = (p1.getX() < p2.getX() ? p1.getX():p2.getX()) ;
		int tlY = (p1.getY() < p2.getY() ? p1.getY():p2.getY()) ;
		setWidth() ;// make sure the width is up to date
		setHeight() ; //make sure height is up to date
		if(x >= tlX && x <= tlX + this.getWidth())
		{
			if(y >= tlY && y <= tlY + this.getHeight())
			{
				notifyObservers();
				return true ;
			}
		}
		notifyObservers();
		return false ;
		
	}

	public void resize(Points anchor, Points move)
	{
		this.p1 = anchor ; 
		this.p2 = move ;
		this.setWidth(Math.abs(p1.getX()-p2.getX()));
		this.setHeight(Math.abs(p1.getY()-p2.getY()));
		notifyObservers();
	}
	
	private Points getTLPoint()
	{
		int x = (p1.getX() < p2.getX() ? p1.getX():p2.getX()) ;
		int y = (p1.getY() < p2.getY() ? p1.getY():p2.getY()) ;
		return new Points(x,y) ;
	}
	
	public Points getCenterPT()
	{
		setWidth() ; //ensure that width and height are updated
		setHeight() ;
		
		Points tl = getTLPoint() ; // get upper left corner of bounds
		int cx = tl.getX() + (getWidth()/2) ;//get center x position
		int cy = tl.getY() + (getHeight()/2) ;//get center y position
		notifyObservers();
		return new Points(cx,cy);//return the center point;
	}
	@Override
	public void setPosition(int x, int y)
	{
		p1 = new Points(p1.getX() + x, p1.getY() + y) ;
		this.setX(p1.getX());
		this.setY(p1.getY());
		p2 = new Points(p2.getX() + x, p2.getY() + y) ;
		notifyObservers() ;
	}
	public void setID(int i)
	{
		super.setID(i);
		notifyObservers() ;
	}
	public void setColor(Color c)
	{
		super.setColor(c);
	}
}
