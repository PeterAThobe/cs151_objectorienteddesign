
public interface Observable 
{
	public void add(ModelListener m) ;
	public void remove(ModelListener m) ;
	public void notifyObservers() ;
}
