import java.awt.Color;
import java.awt.Graphics;

public abstract class DShape implements ModelListener
{
	DShapeModel model;
	protected Canvas canvas ; //allows us to update the canvas
	protected Controls control;
	
	public static final int KNOB_SIZE = 10 ;
	public static final Color KNOB_COLOR = Color.BLACK ;
	
	public boolean drawKnobs = false; //used if currentlySelected
	protected Points[] knobs;
	public  boolean moveable = false ;
	
	public DShape(DShapeModel m,Canvas canvas, Controls control)
	{
		this.model = m ;
		this.canvas = canvas ;
		this.control = control ;
		this.model.add(this) ; // Add observer DShape
		this.model.add(canvas) ; // Add observer Canvas panel
		this.model.add(control) ; // Add observer Controls panel
		this.model.add(new TextObserver(control,canvas));
		this.moveable = false ;
		this.model.notifyObservers();
	}
	public abstract void draw(Graphics g);
	public abstract String getClassName();
	public DShapeModel getModel() {return this.model ;}
	
	public void modelChanged(DShapeModel m) //model Update needs to differ here 
	{
		//do nothing
	}
	public void drawKnobs(Graphics g)
	{
		if(drawKnobs)
		{
			this.setKnobs();
			int d = KNOB_SIZE ;
			g.setColor(KNOB_COLOR);
			for(Points p : knobs)
			{
				g.fillRect(p.getX(), p.getY(), d, d) ;
			}
		}
		
	}
	
	 public void setKnobsVisible(boolean t)
	 {
		 this.drawKnobs = t ;
	 }
	 public void setKnobs()
	 {
			 this.knobs = new Points[4];
			 int x = model.getX() ;
			 int y = model.getY() ;
			 int h = model.getHeight() ;
			 int w = model.getWidth() ;
			 int r = KNOB_SIZE/2 ;
			 knobs[0] = new Points(x - r,y - r) ;//topLeft
			 knobs[1] = new Points(x + w - r,y - r) ;//topRIght
			 knobs[2] = new Points(x + w - r,y + h - r) ;//BottomRight
			 knobs[3] = new Points(x - r,y + h - r) ;//BottomLeft	 
	 }
	 
	 public boolean getMoveable()
	 {
		 return this.moveable ;
	 }
	 public void setMoveable(boolean m)
	 {
		 this.moveable = m ;
	 }
	 public boolean checkKnobClicked(int x, int y) //maybe return the clicked knob instead of boolean
	 {
		 this.setKnobs();
		 int r = KNOB_SIZE ;
		 for(Points p : knobs)
		 {
			if(p.checkCollision(r, new Points(x,y)) )
			{
				return true;
			}
		 }
		 return false;
	 }
	 public void setModel(DShapeModel m)
	 {
		 this.model = m ;
	 }
	 public Points getCLickedPoint(Points p)
	 {
		 this.setKnobs();
		 if(checkKnobClicked(p.getX(),p.getY()))
		 {
			 setMoveable(true); //moveable true = resize necessary
			 for(Points k : knobs)
			 {
				 if(k == p)
				 {
					 return k;
				 }
			 }
		 }
		 return null;
	 }
	 public Points getAnchor(Points p)
	 {
		 this.setKnobs();
		 int i = 0 ;
		 int j = 0;
		 for(i = 0  ; i < knobs.length ; i++)
		 {
			 if(knobs[i] == p)
			 {
				 j = i;
				 break;
			 }
		 }

		 switch(j)
		 {
		 case 0:
			 i = 2 ;
			 break;
		 case 1:
			 i = 3 ;
			 break;
		 case 2:
			 i = 0 ;
			 break;
		 case 3:
			 i = 1 ;
			 System.out.println(i);
			 break;
		 }
		 return knobs[i];
	 }
	 public void resize(Points m, Points a)
	 {
		 model.resize(a, m);
		 model.notifyObservers();
	 }
	 
	 public boolean getdrawKnobs()
	 {
		 return this.drawKnobs ;
	 }
	 
	 public void updateModel(DShapeModel newModel)
	 {
		 this.model = newModel;
		 this.model.notifyObservers();
	 }
}
