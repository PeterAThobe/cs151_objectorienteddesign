import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;

public class DLine extends DShape
{

	public DLine(Canvas canvas, Controls control) 
	{
		super(new DLineModel(), canvas, control) ;
		this.knobs = new Points[2] ;
		setKnobs();
		model.notifyObservers();
	}

	@Override
	public void drawKnobs(Graphics g) 
	{
		if(drawKnobs)
		{
			this.setKnobs();
			g.setColor(KNOB_COLOR);
			for(Points k: knobs)
			{
				g.fillRect(k.getX(), k.getY(), KNOB_SIZE, KNOB_SIZE);
			}
		}
	}
	@Override
	public void setKnobs() 
	{
		DLineModel line = (DLineModel)getModel();
		int r = KNOB_SIZE/2;
		knobs[0] = new Points(line.getP1().getX()-r,line.getP1().getY()-r) ;
		knobs[1] = new Points(line.getP2().getX()-r,line.getP2().getY()-r) ;
	}
	@Override
	public void draw(Graphics g) 
	{
		Graphics2D g2 = (Graphics2D)g ;
		DLineModel line = (DLineModel)getModel();
		 g2.setColor(model.getColor());
		 g2.setStroke(new BasicStroke(3));
		 g2.drawLine(line.getP1().getX(), line.getP1().getY(), 
				 	line.getP2().getX(), line.getP2().getY()) ;
		 if(drawKnobs)
		 {
			 drawKnobs(g) ;
		 }
	}

	@Override
	public String getClassName() 
	{
		return "Line" ;
	}
	
	@Override
	public boolean checkKnobClicked(int x, int y) 
	{
		return super.checkKnobClicked(x, y);
	}
	
	public Points getAnchor(Points p)
	 {
		 this.setKnobs();
		 int i = 0 ;
		 int j = 0;
		 for(i = 0  ; i < knobs.length ; i++)
		 {
			 if(knobs[i] == p)
			 {
				 j = i;
				 break;
			 }
		 }

		 switch(j)
		 {
		 case 0:
			 i = 1 ;
			 break;
		 case 1:
			 i = 0 ;
			 break;
		 
		 }
		 return knobs[i];
	 }
}
