import java.awt.Graphics;

public class DRectangle extends DShape
{
	
	
	public DRectangle(Canvas canvas,Controls control)
	{
		super(new DRectangleModel(),canvas, control) ;
		model.notifyObservers();
	}
	
	@Override
	public void draw(Graphics g) 
	{
		g.setColor(model.getColor());
		g.fillRect(model.getX(), model.getY(), model.getWidth(),model.getHeight()) ;
		if(drawKnobs)
		{
			drawKnobs(g);
		}
	}
	
	
	@Override
	public String getClassName() 
	{
		return "Rectangle" ; 
	}



}
