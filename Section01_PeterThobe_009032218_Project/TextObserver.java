
public class TextObserver implements ModelListener
{
	private Controls control;
	private DShape shape;
	private Canvas canvas ;
	
	public TextObserver(Controls c, Canvas canvas)
	{
		this.control = c ;
		this.canvas = canvas ;
	}

	@Override
	public void modelChanged(DShapeModel model) 
	{	
		if(canvas.getSelected() != null)
		{
			if((canvas.getSelected().getModel() instanceof DTextModel))
			{
				control.setTextfunctionality(true);
				DTextModel m = (DTextModel)canvas.getSelected().getModel() ;
				control.setFontType(m.getFontName()) ;
				control.changeText(m.getText()) ;
			}
			else
			{
				control.setTextfunctionality(false);
			}
		}
		
	}

}
