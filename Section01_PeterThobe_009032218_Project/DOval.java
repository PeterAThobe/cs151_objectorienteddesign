import java.awt.Graphics;

public class DOval extends DShape
{

	public DOval(Canvas canvas,Controls control) 
	{
		super(new DOvalModel(),canvas, control) ;
		model.notifyObservers();
	}

	@Override
	public void draw(Graphics g) 
	{
		g.setColor(model.getColor()) ;
		g.fillOval(model.getX(),model.getY(), model.getWidth(), model.getHeight()) ;
		if(drawKnobs)
		{
			drawKnobs(g);
		}
	}

	
	@Override
	public String getClassName() {return "Oval" ; }



}
