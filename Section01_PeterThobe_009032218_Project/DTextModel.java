import java.awt.Color;
import java.awt.Font;

public class DTextModel extends DShapeModel
{
	private String text ;
	private String font ;

	
	public DTextModel()
	{
		super();
		font = "Dialog" ;
		text = "Hello!" ;
		super.setWidth(100);
		super.setX(100);
		super.setY(100);
	}
	public String getText()
	{
		return this.text ;
	}
	public void setText(String t)
	{
		this.text = t ;
		notifyObservers();
	}
	public void setFontName(String f)
	{
		this.font = f ;
		notifyObservers();
	}
	public String getFontName()
	{
		return this.font;
	}
	
	
	public void setID(int i)
	{
		super.setID(i);
		notifyObservers() ;
	}
	public void setColor(Color c)
	{
		super.setColor(c);
	}
}
