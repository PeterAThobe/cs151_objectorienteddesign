
public class Matrix 
{
	private int [][] matrix ;
	private int row	;
	private int column	;
	
	Matrix(int row, int col)
	{
		matrix = new int [row][col]	;
		this.row = row	;
		this.column = col	;
		
		for(int i = 0 ;  i < row ; i++)
		{
			for(int j = 0 ; j < col ; j++)
			{
				matrix[i][j] = 0;
			}
		}
	}
	
	public int getRow()
	{
		return row	;
	}
	
	public int getColumn()
	{
		return column	;
	}
	
	@SuppressWarnings("null")
	public int getValueAt(int r, int c)
	{
		if(r < this.row && c < this.column)
		{
			return matrix[r][c]	;
		}
		return (Integer) null	;
	}
	
	public void setValueAt(int r,int c, int n)
	{
		if(r < this.row && c < this.column)
		{
			matrix[r][c]	 = n;
		}
	}
	
	public static Matrix add(Matrix a, Matrix b)
	{
		Matrix c	;
		
		if(a.getRow() == b.getRow() && a.getColumn() == b.getColumn())
		{
			int row = a.getRow()	;
			int col = a.getColumn()	;
			c = new Matrix(row,col)	;
			for(int i = 0 ; i < row ; row++)
			{
				for(int j = 0 ; j < col ; col++)
				{
					int sum = a.getValueAt(i, j)	+ b.getValueAt(i, j)	;
					c.setValueAt(i, j, sum);
				}
			}
			return c;
		}
		
		return null	;
	}
	
	public String getString()
	{
		String str = ""	;
		for(int i =0 ; i < this.getRow() ; i++)
		{
			for(int j = 0 ; j < this.getColumn()	; j++)
			{
				str = str.concat(this.getValueAt(i, j) + "\t")	;
			}
			
			str = str.concat("\n")	;
		}
		return str	;
	}
	
	public Matrix multiply(int n)
	{
		Matrix m = new Matrix(this.row, this.column)	;
		for(int i = 0 ; i < this.row ; i++)
		{
			for(int j = 0 ; j < this.column ; j++)
			{
				int num = n * this.getValueAt(i, j) 	;
				m.setValueAt(i, j, num)	;
			}
		}
		return m		;
	}
	
	public int getDiagonalSum()
	{
		if(this.row == this.column)
		{
			int sum = 0	;
			for(int i = 0 ; i < this.row ; i++)
			{
				for(int j = 0 ; j < this.column ; j++)
				{
					if(i == j)
					{
						sum += this.getValueAt(i, j)	;
					}
				}
			}
			return sum	;
		}
		else 
		{
			return Integer.MIN_VALUE	;
		}
	}
}
