public class StringCode 
{
	/**
	 * 
	 * @param str Reference String to Blow Up
	 * @return Blown Up String
	 */
	public static String blowUp(String str)
	{
		String blownUp = ""	;
		int length = str.length()	;
		for(int i = 0 ; i < length ; i++)
		{
			int expandBy	 = 0;
			if( Character.isDigit( str.charAt(i) ) )
			{
				if( i != length - 1)
				{	String temp = ""	;
					expandBy = Character.digit(str.charAt(i),10)	;		
					for(int j = 0 ; j < expandBy  ; j++)
					{
						temp = temp.concat( Character.toString(str.charAt(i+1)))	;
					}
					
					blownUp = blownUp.concat(temp)	;
				}
			}
			else
			{
				blownUp = blownUp.concat(Character.toString(str.charAt(i)))	;
			}
		}
		
		return blownUp	;
	}
	
	
	/**
	 * 
	 * @param str Reference String to parse
	 * @return	Length of largest run
	 */
	public static int maxRun(String str)
	{
		int length =  str.length()	;
		int maxRun = 0	;
		int currentRun = 0 	;
		char charComparator = str.charAt(0) 	;
		for(int i = 0 ; i < length ; i++)
		{
			if(charComparator == str.charAt(i))
			{
				currentRun++	;
			}
			
			else
			{
				charComparator = str.charAt(i)	;
				if(currentRun > maxRun)
				{
					maxRun = currentRun	;
				}
				currentRun = 1	;
				charComparator = str.charAt(i)	;
			}
		}
		
		return maxRun	;	
	}
}
